#!/usr/bin/perl

use strict;
use warnings;
#use diagnostics;

# Standard
use Getopt::Long;
use Data::Dumper;
use Term::ANSIColor;
use File::Glob;

# dependance
use Error qw( :try );

use FindBin qw($Bin);

# ------------------
use lib "$Bin/../lib";
use lib "$Bin/../foreignLib";

use AgpLoader;

our $VERSION = 1.0;
use constant DEBUG => 1;

{
	my( $outdir );

	sub setOutdir
	{	$outdir = shift;	}

	sub getFileObject
	{
		my( $subdir, $obj ) = @_;

		my $dir = "$outdir/$subdir";
		mkdir $dir unless( -d $dir );

		my $filepath = "$dir/chromosome_$obj.$subdir";
		my $o_file   = Safe::newIOFile( $filepath, 'w' );
		print $o_file join( "\t", qw( element start end ) ), "\n";

		return $o_file;
	}
}


MAIN:
{
	try
	{
		&Main();
	}
	otherwise
	{
		my ( $o_err, $r ) = @_;

		my $str = $o_err->file;
		$str   .= ' line ' . $o_err->line;
		$str   .= "\n" . $o_err->text . "\n";

		warn $str;
	};

	exit( 0 );
}


sub Main
{
	my $rh_options = &defineOptions();
	&debug( $rh_options );

	&analyseOptions( $rh_options );
	&debug( $rh_options );

	my( $indir, $outdir ) = @$rh_options{ qw( agp_dir outdir ) };
	setOutdir( $outdir );

	for my $agp_file ( <$indir/*.agp> )
	{
		my $o_loader = new AgpLoader( $agp_file )->load();
		my $rh_agp_objects = $o_loader->getObjects();
		generateFiles( $rh_agp_objects, $$rh_options{ verbose } );
	}

	exit( 0 );
}


sub generateFiles
{
	my( $rh_agp_objects, $verbose ) = @_;

	my @a_names = keys %$rh_agp_objects;
	my $msg = "Found " . scalar( @a_names ). " AGP object(s)\n";
	$msg   .= "-@a_names-\n";
	warn $msg if( $verbose );

	# when I wrote this code, there was only one AgpObject per hash
	for my $name ( sort keys %$rh_agp_objects )
	{
		warn "working with $name ...\n" if( $verbose );
		my $o_agp_obj = $$rh_agp_objects{ $name };

		my( $o_fs );

		my $ra_gaps   = $o_agp_obj->getGaps();
		if( 0 != @$ra_gaps )
		{
			$o_fs = getFileObject( 'ocean', $name );
		}
		writeOceans( $o_fs, $ra_gaps, $verbose );
		$o_fs->close();

		my $ra_compos   = $o_agp_obj->getComponents();
		if( 0 != @$ra_compos )
		{
			$o_fs = getFileObject( 'contig', $name );
		}
		writeContigs( $o_fs, $ra_compos, $verbose );
		$o_fs->close();


		my $ra_clones = $o_agp_obj->getClones();
		if( 0 != @$ra_clones )
		{
			$o_fs = getFileObject( 'clone', $name );
		}
		writeClones( $o_fs, $ra_clones, $verbose );
		$o_fs->close();

		my $ra_supers = $o_agp_obj->getSuperContigs();
		if( 0 != @$ra_supers )
		{
			$o_fs = getFileObject( 'super', $name );
		}
		writeSupers( $o_fs, $ra_supers, $verbose );
		$o_fs->close();
	}

	return;
}


sub writeOceans
{
	my( $o_fs, $ra_gaps, $verbose ) = @_;

	my $i = 0;
	for my $o_gap ( @$ra_gaps )
	{
		my( $start, $end ) = $o_gap->getObjectCoords();

		my @a_data = ( ++$i, $start, $end );
		print $o_fs join( "\t", @a_data ), "\n";
	}
	warn "Oceans : $i lines have been written.\n" if( $verbose );

	return;
}

sub writeContigs
{
	my( $o_fs, $ra_compos, $verbose ) = @_;

	my $i = 0;
	for my $o_compo ( @$ra_compos )
	{
		my( $start, $end ) = $o_compo->getObjectCoords();

		my @a_data = ( ++$i, $start, $end );
		print $o_fs join( "\t", @a_data ), "\n";
	}
	warn "Contigs : $i lines have been written.\n" if( $verbose );

	return;
}

sub writeSupers
{
	my( $o_fs, $ra_supers, $verbose ) = @_;

	my $i = 0;
	for my $o_super ( @$ra_supers )
	{
		my( $start, $end ) = $o_super->getCoords();

		my @a_data = ( ++$i, $start, $end );
		print $o_fs join( "\t", @a_data ), "\n";
	}
	warn "Super-contigs : $i lines have been written.\n" if( $verbose );

	return;
}

sub writeClones
{
	my( $o_fs, $ra_clones, $verbose ) = @_;

	my $i = 0;
	for my $o_clone ( @$ra_clones )
	{
		# ça me déplait fortement que start et end sur le chrom ne se récupèrent pas avec la même methode !
		my ( $start, $end ) = $o_clone->getCoords();

		my @a_data = ( ++$i, $start, $end );
		print $o_fs join( "\t", @a_data ), "\n";
	}
	warn "Clones : $i lines have been written.\n" if( $verbose );

	return;
}


sub analyseOptions
{
	my( $rh_options ) = @_;

	for my $key ( qw( agp_dir outdir ) )
	{
		my $dir = $$rh_options{ $key };
		if( !-d $dir )
		{
			my $msg = "$dir is not a directory.\n";
			$msg   .= "Check your $key option.\n";
			throw Error( -text => $msg );
		}
	}

	return;
} # analyseOptions


sub defineOptions
{
	Getopt::Long::Configure( qw( auto_help auto_version ) );

	my @a_options = qw( agp_dir=s outdir=s verbose );
	my $rh_options = { outdir => '.' };
	GetOptions( $rh_options, @a_options );

	#mandatory
	my @a_mand_keys = qw( agp_dir );
	&__assertMandatory( $rh_options, \@a_mand_keys );

	return $rh_options;

	sub __assertMandatory
	{
		my( $rh_options, $ra_keys ) = @_;

		foreach my $key ( @$ra_keys )
		{
			if( !defined $$rh_options{ $key } )
			{
				&__usage();

				my $msg = "\n\t*$key* is a mandatory attribute, you have to set it.\n\n";
				die( $msg );
			}
		}
	}

	sub __usage
	{

		my $msg = "at least one argument is missing.\n";
		warn colored( [ 'bold blue on_white' ], $msg );

		my $ra_colors = ['bold red'];
		$msg  = "try '" . colored( $ra_colors, "--help" );
		$msg .= "', '" . colored( $ra_colors, "-h" );
		$msg .= "', '" . colored( $ra_colors, "-?" );
		$msg .= "' options to see how to use the program\n";
		warn $msg;

		$msg  = "You can also use '" . colored( $ra_colors, "perldoc $0" );
		$msg .= "' to have complete documentation\n";
		warn $msg;

		return;
	}

} # fin de defineOptions


sub debug
{
	return if( 1 != &DEBUG );

	warn Dumper @_;

	return;
}


__END__

=head1 NAME

 writeOceanCloneNContigFromAgp.pl - write ocean, clone, and contig data in Nar6 format.

=head1 SYNOPSIS

 writeOceanCloneNContigFromAgp.pl @a_argv [ @a_options ]

 @a_argv :
	--agp_dir => dirpath, path to the directory containing the AGP file.

 @a_options :
	--outdir  => dirpath [.], where to write output.
	--verbose => flag, if set some information will be displayed on STDERR.

=head1 EXAMPLE

 with all default values :
 cat toto.tsv | perl writeOceanCloneNContigFromAgp.pl

 with no default values :
 perl writeOceanCloneNContigFromAgp.pl --infile toto --outfile resultat

=head1 DESCRIPTION

 descr

=head1 AUTHORS

 sebastien.letort@inserm.fr

=cut
