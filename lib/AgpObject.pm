#
# 	seb.letort_job@laposte.net
#	Created: April 16, 2011
#	$Id$
#
package AgpObject;

use strict;
use warnings;

use Data::Dumper;

use Feature;

sub new
{
	my $class = shift;
	my $self = bless {}, $class;

	$self->_init( @_ );

	return $self;
}

sub _init
{
	my $self = shift;
	my( $obj_id ) = @_;

	# $self->{ __rh_elements } = {};
	$self->{ __id }  = $obj_id;
	$self->{ __end } = undef; # last base

	$self->{ __ra_gaps }       = [];
	$self->{ __ra_components } = [];

	$self->{ __ra_clones }     = undef;
	$self->{ __ra_super_contigs } = undef;

	return;
}

sub addElement
{
	my $self = shift;
	my( $o_elt ) = @_;

	# I supposed that element are added in object's order
	# Nothing in my code about that, I relay on the AGP file.
	# Nothing in the spec about that !
	my( $start, $end ) = $o_elt->getObjectCoords();
	$self->{ __end } = $end;

	# my $elt_id = $o_elt->getId();
	# $self->{ __rh_elements }{ $elt_id } = $o_elt;
	if( $o_elt->isa( 'AgpGap' ) )
	{
		my $ra_gaps = $self->{ __ra_gaps };
		push( @$ra_gaps, $o_elt );
	}
	else
	{
		my $ra_compos = $self->{ __ra_components };
		push( @$ra_compos, $o_elt );
	}

	return $self;
}

sub getGaps
{
	my $self = shift;

	my $ra_gaps = $self->{ __ra_gaps };

	# in a good AGP file, gaps will already be ordered by position
	# but this is not in the specification
	# maybe one day, a sort function should be introduce.
	# I will not anticipate.

	return $ra_gaps;
}

sub getComponents
{
	my $self = shift;

	my $ra_compos = $self->{ __ra_components };

	# in a good AGP file, gaps will already be ordered by position
	# but this is not in the specification
	# maybe one day, a sort function should be introduce.
	# I will not anticipate.

	return $ra_compos;
}

sub getSuperContigs
{
	my $self = shift;

	# a super-contig is a group of continuous contigs.

	# do we have a reason to keep them in memory ?
	my $ra_supers = $self->{ __ra_super_contigs };
	return $ra_supers if( defined $ra_supers );

	# compute it if not already done.
	my $ra_contigs = $self->getComponents();
	$ra_supers     = [];
	return $ra_supers unless( 1 < @$ra_contigs );

	my $nb = 1;
	my( $start, $end ) = $$ra_contigs[0]->getObjectCoords();

	for( my $j = 1; $j < @$ra_contigs; ++$j )
	{
		my( $j_start, $j_end ) = $$ra_contigs[$j]->getObjectCoords();
		if( ( $end + 1 ) == $j_start )
		{
			++$nb;
			$end = $j_end;
			next;
		}

		if( 1 < $nb )
		{
			# my( $start, $toto ) = $a_contigs_in_super[0]->getObjectCoords();
			# my( $tata, $end )   = $a_contigs_in_super[-1]->getObjectCoords();
			my $o_super = new Feature( $start, $end );
			push( @$ra_supers, $o_super );
		}

		$nb = 1;
		( $start, $end ) = ( $j_start, $j_end );
	}

	# last
	if( 1 < $nb )
	{
		my $o_super = new Feature( $start, $end );
		push( @$ra_supers, $o_super );
	}

	# my( $start, $end );
	# for my $o_contig ( @$ra_contigs )
	# {
		# my( $s_start, $s_end ) = $o_contig->getObjectCoords();
		# if( !defined $start )
		# {
			# ( $start, $end ) = ( $s_start, $s_end );
			# next;
		# }

		# if( $s_start == ( $end + 1 ) )
		# {
			# $end = $s_end;
			# next;
		# }

		# # let's store the previous data
		# my $o_super = new Feature( $start, $end );
		# push( @$ra_supers, $o_super );

		# ( $start, $end ) = ( $s_start, $s_end );
	# }

	# # let's store the previous data
	# if( defined $start )
	# {
		# my $o_super = new Feature( $start, $end );
		# push( @$ra_supers, $o_super );
	# }

	$self->{ __ra_super_contigs } = $ra_supers;

	return $ra_supers;
}

sub getClones
{
	my $self = shift;

	# a clone starts at 1 on after a clone gap
	# it ends before a clone gap or at the end of the last contig

	my $ra_clones = $self->{ __ra_clones };
	return $ra_clones if( defined $ra_clones );

	# compute it if not already done.
	my $ra_gaps = $self->getGaps();
	my $start   = 1;
	$ra_clones  = [];

	my( $c_start, $c_end );
	for my $o_gap ( @$ra_gaps )
	{
		next unless $o_gap->getType() eq "clone";

		( $c_start, $c_end ) = $o_gap->getObjectCoords();
		my $o_clone = new Feature( $start, $c_start - 1 );
		push( @$ra_clones, $o_clone );

		$start = $c_end + 1;
	}

	# If the last element is a gap, there's nothing left to add.
	if( $c_end != $self->{ __end } )
	{
		my $o_clone = new Feature( $start, $self->{ __end } );
		push( @$ra_clones, $o_clone );
	}
	$self->{ __ra_clones } = $ra_clones;

	return $ra_clones;
}

# sub getElements
# {
	# my $self = shift;

	# return $self->{ __rh_elements };
# }

sub getId
{
	my $self = shift;

	return $self->{ __id };
}


1;

__END__

=pod

=head1 Licence

 Feature.pm - a class that manages features in biological sequence (ie coords and strand).
 Copyright (C) 2011 Sébastien Letort

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

 seb.letort_job@laposte.net

=head1 NAME

 Feature - a class that manages features in biological sequence (ie coords and strand).

=head1  SYNOPSIS

 # a feature is anything on biological sequence that have a start, a end, and sometime a strand.
 my $o_feature = new Feature( $start, $end, $strand );

 # get start and end
 my( $f_start, $f_end ) = $o_feature->getCoords();

 # get strand
 my $f_strand = $o_feature->getStrand();

=head1 DESCRIPTION

 This class manages feature.
 A feature is anything that can be placed onto a biological sequence,
 ie anything with a start, a end and sometime a strand.
 For a strict use a this class, you should precise the sequence where this feature belongs.

 It provides 2 functions to access its 3 attributes.
 getCoords and getStrand.

 This class aims to be inherited to specific features like SNP, gene, exons, ...

=head1 METHODS

=head2 new / _init

 Usage     : my $o_x = new Feature( $start, $end, $strand );
 Fonction  : initialise an instance of Feature class.
 Returns   : a valid Feature object
 Args      :
	$start,  integer, where the feature begins.
	$end,    integer, where the feature ends.
	$strand, enum( 1,-1 ), optionnal, 1 for positive strand, -1 for negative strand
 Note      : Those coordinates are 1-based and inclusive.
	Which means that the line
		$o_feat = new Feature( 1,100, 1 );
	will create a feature that begins on the very first base of the sequence,
	that it ends on the 100th base of the sequence,
	which make it a 100 bases long feature.
 Attributes   :
	__start,  integer, where the feature begins.
	__end,    integer, where the feature ends.
	__strand, enum( 1,-1 ), optionnal, 1 for positive strand, -1 for negative strand

=head2 getCoords

 Usage     : ( $start, $end ) = $o_x->getCoords();
 Procedure : returns the start and the end of the feature.
 Returns   : a list of 2 integers (which can be undef).
 Args      : none.

=head2 getStrand

 Usage     : $strand = $o_x->getStrand();
 Procedure : returns the strand of the feature.
 Returns   : a scalar, 1, -1 or undef.
 Args      : none.

=cut
