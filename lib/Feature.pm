#
# 	seb.letort_job@laposte.net
#	Created: April 16, 2011
#	$Id$
#

package Feature;

use strict;
use warnings;

use Data::Dumper;

sub new
{
	my $class = shift;

	my $self  = bless( {}, $class );
	$self->_init( @_ );

	return $self;
}

sub _init
{
	my $self = shift;
	my( $start, $end, $strand ) = @_;

	my @a_attrs = qw( __start __end __strand );
	@$self{ @a_attrs } = ( $start, $end, $strand );

	return;
}

sub getCoords
{
	my $self = shift;

	my @a_attrs = qw( __start __end );
	return @$self{ @a_attrs };
}

# est-ce vraiment utile ?
# sub getLength
# {
	# my $self = shift;
#
	# my( $start, $end ) = $self->getCoords();
# }

sub getStrand
{
	my $self = shift;

	return $self->{ __strand };
}


1;

__END__

=pod

=head1 Licence

 Feature.pm - a class that manages features in biological sequence (ie coords and strand).
 Copyright (C) 2011 Sébastien Letort

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

 seb.letort_job@laposte.net

=head1 NAME

 Feature - a class that manages features in biological sequence (ie coords and strand).

=head1  SYNOPSIS

 # a feature is anything on biological sequence that have a start, a end, and sometime a strand.
 my $o_feature = new Feature( $start, $end, $strand );

 # get start and end
 my( $f_start, $f_end ) = $o_feature->getCoords();

 # get strand
 my $f_strand = $o_feature->getStrand();

=head1 DESCRIPTION

 This class manages feature.
 A feature is anything that can be placed onto a biological sequence,
 ie anything with a start, a end and sometime a strand.
 For a strict use a this class, you should precise the sequence where this feature belongs.

 It provides 2 functions to access its 3 attributes.
 getCoords and getStrand.

 This class aims to be inherited to specific features like SNP, gene, exons, ...

=head1 METHODS

=head2 new / _init

 Usage     : my $o_x = new Feature( $start, $end, $strand );
 Fonction  : initialise an instance of Feature class.
 Returns   : a valid Feature object
 Args      :
	$start,  integer, where the feature begins.
	$end,    integer, where the feature ends.
	$strand, enum( 1,-1 ), optionnal, 1 for positive strand, -1 for negative strand
 Note      : Those coordinates are 1-based and inclusive.
	Which means that the line
		$o_feat = new Feature( 1,100, 1 );
	will create a feature that begins on the very first base of the sequence,
	that it ends on the 100th base of the sequence,
	which make it a 100 bases long feature.
 Attributes   :
	__start,  integer, where the feature begins.
	__end,    integer, where the feature ends.
	__strand, enum( 1,-1 ), optionnal, 1 for positive strand, -1 for negative strand

=head2 getCoords

 Usage     : ( $start, $end ) = $o_x->getCoords();
 Fonction  : returns the start and the end of the feature.
 Returns   : a list of 2 integers (which can be undef).
 Args      : none.

=head2 getStrand

 Usage     : $strand = $o_x->getStrand();
 Fonction  : returns the strand of the feature.
 Returns   : a scalar, 1, -1 or undef.
 Args      : none.

=cut
