#
# 	seb.letort_job@laposte.net
#	Created: April 16, 2011
#	$Id$
#
package AgpElement;

use strict;
use warnings;

use Data::Dumper;

sub new
{
	my $class = shift;
	my $self = bless {}, $class;

	$self->_init( @_ );

	return $self;
}

sub _init
{
	my $self = shift;
	my( $ra_elts ) = @_;

	# 9 cols :
	#	object_id object_start object_end
	#	part_num comp_type
	#	if comp_type != 'N'
	#		comp_id comp_beg comp_end orientation
	#	else
	#		gap_length gap_type linkage empty_field
	if( 9 != @$ra_elts )
	{
		warn "Line does not have 9 elements.\n";
		die Dumper $ra_elts;
	}

	my @a_object = qw( __object_id __object_start __object_end );
	my @a_compo1 = qw( __part_num __comp_type );
	@$self{ @a_object } = @$ra_elts[ 0..2 ];
	@$self{ @a_compo1 } = @$ra_elts[ 3..4 ];

	# a definir dans classe fille
	$self->_setElementFeature( @$ra_elts[5..8] );

	return;
}

sub getObjectCoords
{
	my $self = shift;

	my @a_attr = qw( __object_start __object_end );
	return @$self{ @a_attr };
}

sub getObjectId
{
	my $self = shift;

	return $self->{ __object_id };
}

sub getId()
{
	my $self = shift;

	return $self->{ __part_num };
}


package AgpComponent;

use strict;
use warnings;

use Data::Dumper;

use base qw( AgpElement );

sub _setElementFeature
{
	my $self = shift;
	my( $comp_id, $comp_beg, $comp_end, $ori ) = @_;

	my @a_attr = qw( __id __start __end __strand );
	@$self{ @a_attr } = ( $comp_id, $comp_beg, $comp_end, $ori );

	return;
}

package AgpGap;

use strict;
use warnings;

use Data::Dumper;

use base qw( AgpElement );


sub _setElementFeature
{
	my $self = shift;
	my( $gap_length, $gap_type, $linkage, $empty_field ) = @_;

	my @a_attr = qw( __length __type __linkage );
	@$self{ @a_attr } = ( $gap_length, $gap_type, $linkage );

	return;
}

sub getLength
{
	my $self = shift;

	return $self->{ __length };
}

sub getType
{
	my $self = shift;

	return $self->{ __type };
}


1;

__END__

=pod

=head1 Licence

 Feature.pm - a class that manages features in biological sequence (ie coords and strand).
 Copyright (C) 2011 Sébastien Letort

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

 seb.letort_job@laposte.net

=head1 NAME

 Feature - a class that manages features in biological sequence (ie coords and strand).

=head1  SYNOPSIS

 # a feature is anything on biological sequence that have a start, a end, and sometime a strand.
 my $o_feature = new Feature( $start, $end, $strand );

 # get start and end
 my( $f_start, $f_end ) = $o_feature->getCoords();

 # get strand
 my $f_strand = $o_feature->getStrand();

=head1 DESCRIPTION

 This class manages feature.
 A feature is anything that can be placed onto a biological sequence,
 ie anything with a start, a end and sometime a strand.
 For a strict use a this class, you should precise the sequence where this feature belongs.

 It provides 2 functions to access its 3 attributes.
 getCoords and getStrand.

 This class aims to be inherited to specific features like SNP, gene, exons, ...

=head1 METHODS

=head2 new / _init

 Usage     : my $o_x = new Feature( $start, $end, $strand );
 Fonction  : initialise an instance of Feature class.
 Returns   : a valid Feature object
 Args      :
	$start,  integer, where the feature begins.
	$end,    integer, where the feature ends.
	$strand, enum( 1,-1 ), optionnal, 1 for positive strand, -1 for negative strand
 Note      : Those coordinates are 1-based and inclusive.
	Which means that the line
		$o_feat = new Feature( 1,100, 1 );
	will create a feature that begins on the very first base of the sequence,
	that it ends on the 100th base of the sequence,
	which make it a 100 bases long feature.
 Attributes   :
	__start,  integer, where the feature begins.
	__end,    integer, where the feature ends.
	__strand, enum( 1,-1 ), optionnal, 1 for positive strand, -1 for negative strand

=head2 getCoords

 Usage     : ( $start, $end ) = $o_x->getCoords();
 Procedure : returns the start and the end of the feature.
 Returns   : a list of 2 integers (which can be undef).
 Args      : none.

=head2 getStrand

 Usage     : $strand = $o_x->getStrand();
 Procedure : returns the strand of the feature.
 Returns   : a scalar, 1, -1 or undef.
 Args      : none.

=cut
