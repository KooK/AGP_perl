#
# 	seb.letort_job@laposte.net
#	Created: April 16, 2011
#	$Id$
#
package AgpLoader;

use strict;
use warnings;

use Data::Dumper;

use FileIterator::Agp;
use AgpElement; # contains AgpComponent AgpGap
use AgpObject;

sub new
{
	my $class = shift;
	my $self = bless {}, $class;

	$self->_init( @_ );

	return $self;
}

sub _init
{
	my $self = shift;
	my( $file ) = @_;

	my $o_reader = new FileIterator::Agp( file => $file );
	$self->{ __o_reader }  = $o_reader;

	$self->{ __rh_objects } = {};
	$self->{ __nb_objects } = 0;

	return;
}

sub load
{
	my $self = shift;

	my $o_reader = $self->{ __o_reader };
	while( my $ra_elts = $o_reader->next() )
	{
		# 9 cols :
		#	object_id object_start object_end
		#	part_num comp_type
		#	if comp_type != 'N'
		#		comp_id comp_beg comp_end orientation
		#	else
		#		gap_length gap_type linkage empty_field
		my $o_elt;
		if( $$ra_elts[4] eq 'N' )
		{
			$o_elt = new AgpGap( $ra_elts );
		}
		else
		{
			$o_elt = new AgpComponent( $ra_elts );
		}

		my $o_object = __getAgpObject( $self, $o_elt->getObjectId() );
		$o_object->addElement( $o_elt );
	}

	return $self;
}

# return the AgpObject with the id $obj_id
sub __getAgpObject
{
	my $self = shift;
	my( $obj_id ) = @_;

	my $o_obj = $self->{ __rh_objects }{ $obj_id };
	if( !defined $o_obj )
	{
		$o_obj = new AgpObject( $obj_id );
		$self->{ __rh_objects }{ $obj_id } = $o_obj;
		$self->{ __nb_objects } += 1;
	}

	return $o_obj;
}

sub getNbObjects
{
	my $self = shift;

	return $self->{ __nb_objects };
}

sub getObjects
{
	my $self = shift;

	return $self->{ __rh_objects };
}


1;

__END__

=pod

=head1 Licence

 Feature.pm - a class that manages features in biological sequence (ie coords and strand).
 Copyright (C) 2011 Sébastien Letort

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

 seb.letort_job@laposte.net

=head1 NAME

 AgpLoader - a class that can parse AGP files.

=head1  SYNOPSIS

 my $o_loader = new AgpLoader( $agp_file );
 $o_loader->load();

 # you can access the AgpObject that composed the file.
 warn "Found " . $o_agp->getNbObjects(). " AGP objects\n";
 my $rh_objects = $o_agp->getObjects();

=head1 DESCRIPTION

 This class use FileIterator::Agp to parse AGP files.
 AGP is a file format that describe sequence assembly.
 The specification can be found at http://www.ncbi.nlm.nih.gov/projects/genome/assembly/agp/AGP_Specification.shtml
 This class deals with v. 1.1

 For each line read, it builds a AgpGap or a AgpComonent, regarding the 5th elements.

 I try to follow only the specification, but not the recommandations.
 So as the spec. do not say that the file should be ordered,
 I do not consider it ordered.

 It provides 1 function to have the number of AgpObject build,
 and another to get those AgpObject.
 getNbObjects and getObjects.

=head1 METHODS

=head2 new / _init

 Usage     : my $o_x = new AgpLoader( $agp_file );
 Fonction  : initialise an instance of AgpLoader class.
 Returns   : a valid AgpLoader object
 Args      :
	$agp_file, filepath or filehandle, the file to parse.
 Note      : The file is not loaded by this constructor.
 Attributes:
	__o_reader,   FileIterator::Agp object, the file iterator used to read the file.
	__rh_objects, hash ref of AgpObjects, keys are object names.
	__nb_objects, integer, the number of AgpObjects this object contains.

=head2 load

 Usage     : $o_x->load();
 Fonction  : load the whole AGP file in memory as AgpObject objects.
 Returns   : the object itself.
 Args      : none.

=head2 getNbObjects

 Usage     : $n = $o_x->getNbObjects();
 Fonction  : returns the number of AgpObject objects that have been build.
 Returns   : an integer (>=0).
 Args      : none.

=head2 getObjects

 Usage     : $rh_object = $o_x->getObjects();
 Fonction  : returns the hash ref containing the AgpObject objects.
 Returns   : a hash ref.
	Keys are AgpObject ids,
	Values are AgpObject objects.
 Args      : none.

=cut

