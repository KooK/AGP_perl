
package FileIterator;

use strict;
use warnings;

use Data::Dumper;
use IO::File;

#Inserm
use Safe;

our $version = 1.0;

sub new
{
	my( $class, @a_args ) = @_;

	my $self = bless( {}, $class );
	$self->_init( @a_args );

	return $self;
}

sub _init
{
	my $self = shift;
	my( $filepath, $code ) = @_;

	my $o_file;
	if( ref( $filepath ) eq '' )
	{
		$o_file = &Safe::newIOFile( $filepath, 'r' );
	}
	else
	{
		$o_file = $filepath;
		$filepath = 'no name';
	}

	my $r_func;
	if( defined $code )
	{
		$r_func = $code;
	}
	else
	{
		# par d�faut on renvoie la line lue.
		$r_func = sub { return shift; };
	}

	my @a_attributs = qw( _filepath _o_file __r_func __line );
	my @a_values    = ( $filepath, $o_file, $r_func, undef );
	@$self{ @a_attributs } = @a_values;

	return;
}

sub next
{
	my $self = shift;

	my( $o_file, $r_func ) = @$self{ qw( _o_file __r_func ) };
	# ne fonctionne pas avec STDIN
	# $self->{ __prev_offset } = $o_file->getpos();
	my $line = <$o_file>;

	return if !defined $line;
	$self->{ __line } = $line;

	# How to deal with mac file : <> di not return a line !
	$line =~ s/\r?\n//;

	return &$r_func( $line );
}

# run after next() !
sub getLine
{
	my $self = shift;

	return $self->{ __line };
}


# sub rewind
# {
	# my $self = shift;

	# my $o_file = $self->{ _o_file };
	# if( !$o_file->isa( 'IO::Seekable' ) )
	# {
		# my $err = "You are trying to rewind a non seekable object.\n";
		# $err   .= "Assert that you are not trying to rewind STDIN.\n";
		# throw LipmError( -text => $err );
	# }

	# $o_file->seek( 0, SEEK_SET );

	# return;
# }


# # trick to allow backward treatement :
# # $self->runParser( $self->getLine() )
# sub runParser
# {
	# my $self = shift;
	# my( $line ) = @_;

	# my $r_func = $self->{ __r_func };
	# return &$r_func( $line );
# }

# sub back
# {
	# my $self = shift;

	# my( $o_file, $offset ) = @$self{ qw( _o_file __prev_offset ) };
	# $o_file->setpos( $offset );

	# return;
# }


sub setParserFunction
{
	my $self = shift;
	my( $r_func ) = @_;

	$self->{ __r_func } = $r_func;

	return;
}

1;

__END__

=head1 CLASS

 FileIterator - An iterator to parse file.

=head1 SYNOPSIS

 my $file = 'foo.ext';
 my $func = sub
	{
		my $line = shift;
		
		return substr( $line, 0, 10 );
	};
 my $o_iter = new FileIterator( $file, $func );
	print "The 10 first letter of each lines :\n";
 while( my $substr = $o_iter->next() )
 {
	print "$substr\n";
 }
 
=head1 DESCRIPTION

 This class aims to clarify the code that parse line by delagating all
 the parsing function to the iterator.

 The constructor defines the 3 protected attributes :
 _filepath, the filepath, if known.
 _o_file,   the io_file object.
 __r_func,  the ref to the func that will parse each line.

=head1 METHODS

=head2 new

 args   :
    $filepath, string or IO:File.
		If $filepath is a string, it is considered as the filepath to open.
		Else, it will be considered as a IO::File object.
		Note : if the filepath end with '.gz', it will be read with IO::Zlib.
	$r_func, ref to a function.
		This function receive a chomped $line as the only argument.
		Its return value will be returned by the next method.
 return : the object.
 exception :
	LipmError if the file cannot be opened.

=head2 next

 No arg.
 return :
	nothing/undef if there is no line left (eof),
	else, the value returned by r_func.

 This method just read the next line, store it and call r_func.

=head2 getLine

 Note   : Run next() before.
 return :
	string, the line as it was read (with its endline).

=head2 setParserFunction

 args   :
	$r_func, ref to a function.

 Set the function that will parse the file.

=head2 _init

 args   :
	cf new
 return :
	nothing.

=head1 AUTHORS

 sebastien.letort@inserm.fr

=head1 RELEASE $Id$

 version 1.0 :
	next return nothing/undef on eof.
	next chomp (s/\r?\n//) the line before call the parsing function.
	getLine return the line as returned by <>, no chomp, no parsing.

=cut
