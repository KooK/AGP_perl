package FileIterator::Agp;

use strict;
use warnings;

use Data::Dumper;

use base qw( FileIterator );

# use LipmError;
use LipmError::Function;
# use Tools;

use constant SEP => "\t";

# mother attribute
#	my @a_attributs = qw( _filepath _o_file );
#
# attributes
# 
sub _init
{
	my $self = shift;
	my %h_args = (
		file     => undef,
		@_ );
	LipmError::Function::Assert( %h_args );

	$self->SUPER::_init( $h_args{ file } );

	my @a_attributes = qw( __line_buffer __rh_header );
	@$self{ @a_attributes } = ( undef, undef );

	# will also set __line_buffer
	# will set a __line attribute by calling mother next method
	#	it will not be used here.
	$self->__parseHeader();

	# parsing the content
	my $r_func = sub{
			my( $line ) = shift;

			$line =~ s/\r?\n//;

			# -1 in split preserve from trimming the empty last element of gap.
			my @a_elts = split( &SEP, $line, -1 );

			# 9 cols :
			#	object_id object_start object_end
			#	part_num comp_type
			#	if comp_type != 'N'
			#		comp_id comp_beg comp_end orientation
			#	else
			#		gap_length gap_type linkage empty_field

			return \@a_elts;
		};
	$self->setParserFunction( $r_func );

	return;
}

sub next
{
	my $self = shift;

	my @a_attr = qw( _o_file __r_func __line_buffer );
	my( $o_file, $r_func, $prev_line ) = @$self{ @a_attr };

	return if !defined $prev_line;

	my $line = <$o_file>;
	$self->{ __line_buffer } = $line;

	return &$r_func( $prev_line );
}


sub __parseHeader
{
	my $self = shift;

	my %h_header = ();
	my $line;

	# here, 'next' just read and chomp
	# this is the mother next
	while( $line = $self->SUPER::next() )
	{
		# chomp in prod only !
		$line =~ s/\r?\n//;

		last if( $line !~ /^#/ );

		# parse header as see in specification example
		# but nothing in the specification describe comment lines.
		my( $key, $val )  = $line =~ /^#\s*(.+)\s*:\s*(.+)\s*/;
		$h_header{ $key } = $val if( defined $val );
	}

	$self->{ __line_buffer } = $line;
	$self->{ __rh_header }   = \%h_header;

	return;
}


1;