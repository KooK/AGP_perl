
# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Thomas.Faraut@toulouse.inra.fr

# This software is a computer program whose purpose is to provide a
# web-based interface for analyzing the different levels of genome
# conservations.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

#
# 	sebastien.letort@toulouse.inra.fr
#	Created: 24-07, 2009
#	$Id: Function.pm 2791 2009-08-25 14:27:28Z sletort $
#

package LipmError::Function;

=pod

=head1 NAME

 LipmError::Function - 

=head1  SYNOPSIS

 &LipmError::Function::Assert( %h_args );

=head1 DESCRIPTION

 #descr

=cut

use strict;
use warnings;

use Data::Dumper;

use base qw( LipmObject );

use LipmError::ParameterException;
#use General;

# =head2 method _Init
# 
#  Usage        : my $o_x = New LipmError::Function( none );
#  Prerequisite : none
#  Fonction     : initialise an instance of LipmError::Function class.
#  Returns      : a valid LipmError::Function object
#  Args         : none
#  Attributes   :
#     __?
# 
# =cut
# 
# sub _Init
# {
# 	my $self = shift;
# 	my %h_args = ( -o_logger => undef,
# 	               none,
# 	               @_ );
# 	$self->SUPER::_Init( %h_args );
# 
# 	# assert mandatories
# #	LipmError::ParameterException::Assert( %h_args );
# 
# 	
# 
# 	return;
# } # _Init


=head2 Procedure Assert

 Usage        : LipmError::Function::Assert( %h_args );
 Prerequisite : static method
 Procedure    : throw a ParameterException error with the 'missing' keyword
                	if one of %h_args values is undef if its key DOES NOT start by '-'.
                Use this procedure to certify that your function have required values.
 Args         : %h_args, any hash
 Error        :	a ParameterException

 Note : This function should replace the ancient ParameterException::Assert function is most of the case.

=cut

sub Assert
{
	my %h_args = @_;

	foreach my $key ( keys %h_args )
	{
		next if( $key =~ /^-/ );

		# I raise the depth of this error not to have an error marked as comming from this file.
		if( !defined $h_args{ $key } )
		{
# 			throw LipmError( -text => ?, -depth => 1 );
			throw LipmError::ParameterException( $key, 'missing',
												 -depth => 1,
												 -text => 'this entry is mandatory. ' );
		}
	}

	return;
}



1;

__END__
