#
# 	sebastien.letort@toulouse.inra.fr
#	Created: May 04, 2007
#	Last Updated: december 03, 2008
#

package LipmError::ParameterException;

=pod

=head1 NAME

 LipmError::ParameterException - a class to define specific exception of Parameter

=head1 SYNOPSIS

 throw LipmError::ParameterException( 'param', 'missing' );
 record LipmError::ParameterException( 'param', 'type',
                    -text => "only integers are allowed for this parameter",
                    -user_msg => "only integers are allowed in the text field" );
 die with LipmError::ParameterException( 'param', 'value',
                                          -should_be => "without figures" );
 catch LipmError::ParameterException with {...}

=head1 DESCRIPTION

 This class deals with parameters exceptions like 'missing', 'wrong-parameter'.
 It has built-in messages, all you have to do is to specify
 which kind of error you have.
 keywords are :
 'missing', for a missing param
 'value', for a wrong value
 'type', quite similar to 'value', if you have string instead of integer for example
 'x', if none of the other keywords fits.

=head1 SUBROUTINES

 new
 _GetDebugMessage
 Assert

=cut

use strict;
use warnings;

## this class inherit from LipmError.pm
use base qw(LipmError);

use Scalar::Util qw( blessed );
BEGIN
{
	our $VERSION = do {my @r = (q$Rev: 2753 $ =~ /\d+/g); $r[0]};
}

# closure to make sure that %_h_msg will not be changed by other classes
# 	should i use constant ?
{

	my %_h_msg = ( 'missing' => " is missing where it is needed.",
	               'type'    => " has a wrong type.",
	               'value'   => " has an incorrect value.",
	               'x'       => "-no information transmitted-"
	             );

	sub _GetMessage
	{
		my ( $keyword ) = @_;
		my $msg = $_h_msg{ $keyword };
		if( !defined $msg )
		{
			throw LipmError( -text => 'Bad keyword used', -depth => 1 );
		}

		return $msg;
	}
}

=head2 Function new

 Title        : new
 Usage        : throw LipmError::ParameterException
                ($param, $keyword [, -should_be => $string, -depth => $depth])
 Prerequisite : it's better for an error to be catch !
 Function     : Constructor
 Returns      :	an Error object that should be catched.
 Args         :	$param, name of the parameter that cause the Error thrown.
              	$keyword, une clef du hash de message predefini
              	$string,	a message explaining what it should be cf Explain().
              	$depth,	to specify how deep the error is thrown.
              		I don't know yet when you have to set it,
              		but (for now) in most cases, just forget it.
 Globals      : none
 Attributes   : _debug, string, a built-in message corresponding to the keyword.
                -should_be, string, explain what the value should be.
                -depth, int, cf LipmError

=cut

sub new
{
	my $class = shift;
	my ($param, $keyword) = (shift, shift);
	my %h_param = ( -should_be => undef,
	                -depth     => 0,
	                @_ );

	# +1 pour ne pas envoyer l'erreur depuis ici, mais depuis
	# la fonction qui l'a jetee.
	local $Error::Depth = $Error::Depth + 1 + $h_param{ -depth };
# 	local $Error::Debug = 1;  # Enables storing of stacktrace

	my $self = $class->SUPER::new( %h_param );
    if (defined ($param) && defined ($keyword))
    {
        #use Data::Dumper;
        #warn Dumper caller();
        #use Carp qw(cluck);
        #cluck('salut');
        $self->{ _debug } = "$param " . _GetMessage($keyword);
    }

	return $self;
}


=head2 function

 Title        : _GetDebugMessage
 Usage        : $msg = $o_xx->_GetDebugMessage();
 Prerequisite : none
 Function     : none
 Returns      : $msg, string
 Args         : none

=cut

sub _GetDebugMessage
{
	my $self = shift;

	my $msg = $self->{ _debug };

	if( defined $self->{ -should_be } )
	{
		$msg .= "It should be $self->{ -should_be } to be correct.\n";
	}

	return $msg;
}


=head2 Function Procedure

 Title        : Assert
 Usage        : LipmError::ParameterException::Assert(
                			'var1'=>$val1, 'var2'=>$val2, -enum => $o_enum );
 Prerequisite : static method
 Procedure    : throw a PE error with the 'missing' keyword
                	if one of %h_vars values is undef.
                If $o_enum is defined, PE with 'value' keyword is thrown.
 Args         : %h_vars, keys are variable names and values are variable value
                -enum is a special key
                $o_enum is an Enum object containing allowed values.
 Globals      : none
 Error        :	a ParameterException

=cut

sub Assert
{
	my %h_vars = @_;

	# to allow the use of the class without Enum class.
	eval( 'use Enum;' );
	my $enum_lib = ( $@ ) ? 0 : 1;

	# the enum option
	my $o_enum = delete $h_vars{ -enum };
	$o_enum = undef	unless( blessed( $o_enum ) && $o_enum->isa( 'Enum' ) );

	# the min-max option
	# TODO

	my @a_keys = keys %h_vars;

	foreach my $var ( @a_keys )
	{
		# I raise the depth of this error not to have an error marked as comming from this file.
		if( !defined $h_vars{ $var } )
		{	throw LipmError::ParameterException($var, 'missing', -depth => 1);	}

		if( ( 1 == $enum_lib ) && defined $o_enum
			&& !$o_enum->Includes( $h_vars{ $var } ) )
		{
			my $list = join ' ', $o_enum->ListItems();
			throw LipmError::ParameterException($var, 'value', -depth => 1, -should_be => "one of the value ( $list )");
		}
	}

	return;
}

1;

__END__

=pod

=head1 COPYRIGHT NOTICE

	This software is governed by the CeCILL license under French law - www.cecill.info
	Copyright INRA/CNRS

	Emmanuel.Courcelle@toulouse.inra.fr
	Jerome.Gouzy@toulouse.inra.fr
	Thomas.Faraut@toulouse.inra.fr

	This software is a computer program whose purpose is to provide a
	web-based interface for analyzing the different levels of genome
	conservations.

	This software is governed by the CeCILL license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	As a counterpart to the access to the source code and  rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author,  the holder of the
	economic rights,  and the successive licensors  have only  limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms.


=head1 AUTHOR

 Sebastien Letort : sebastien.letort@toulouse.inra.fr
 Olivier Stahl    : olivier.stahl@toulouse.inra.fr

=cut
