#
#       sebastien.letort@toulouse.inra.fr
#       Created: september 18, 2007
#       Last Updated: december 03, 2008
#
# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Thomas.Faraut@toulouse.inra.fr

# This software is a computer program whose purpose is to provide a
# web-based interface for analyzing the different levels of genome
# conservations.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

package LipmError;

=pod

=head1 NAME

 LipmError - a class to interface Error classes developped by the lipm team

=head1 SYNOPSIS

 # how to throw LipmError
 use LipmError;

 foo()	or throw LipmError( -text => $debug_msg, -user_msg => $msg_simple );

 #how to catch LipmError
 use Error qw( :try );	# mandatory !
 use LipmError;

 try{ DangerousCode(); }
 catch LipmError with
 {
 	my ( $err, $r ) = @_;

 	# print $msg in a "Lipm" format for developpers
 	print STDERR $err->Explain();

 	# print a message for the user
 	print STDOUT $err->GetUserMessage();
 }
 otherwise
 {
 	my ( $err, $r ) = @_;

 	print STDERR $err->text();
 }
 ; # do not forget it !

=head1 DESCRIPTION

 This class can be used to generate simple Lipm error
 but it is better to use one of its daugther.
 It centralizes what is common with all Error class developped in the Lipm team.
 It declares our global error code and some common methods :
  Explain and _GetBaseMessage

=head1 SUBROUTINES

 new
 IsFatal
 Explain

 __GetBaseMessage
 _GetDebugMessage

=cut

use warnings;
use strict;

# use Data::Dumper;

## this class inherit from Error.pm
use base qw(Error);

# constantes
use constant FATAL => 0001;

BEGIN
{
	our $VERSION = do {	my @r = (q$Rev: 3559 $ =~ /\d+/g); $r[0]	};
}


=head2 Function new

 Title        : new
 Usage        : throw LipmError( -depth => $depth,
                                 -text => $msg, -user_msg => $simple )
 Prerequisite : it's better for an error to be catch !
 Function     : Constructor
 Returns      : an Error object that should be catched.
 Args         : $msg, string	a message for developpers
                $simple, string, a message for users.
                $depth,	to specify how deep the error is thrown.
                	if you want the line incriminated be the one that called the function where the error occur, set 1.
                	if you want it be the one that called the function that called, set 2
                	and so on.
 Globals      : $Error::Depth (?)

 Attributes   :
	#inherited
	-line       int
	-file       string
	-text       string
	-package    string

	_debug      string, build-in message. (no always present. )
	-value      int, error code, odd value should kill the program.
	-user_msg   string, message for a terminal user.

=cut

sub new
{
	my $class = shift;
	my %h_param = ( -depth    => 0,
	                -text     => '',
	                -user_msg => '',
	                -value    => &FATAL,
	                @_
	              );

	local $Error::Depth = $Error::Depth + 1 + $h_param{-depth};

	my $self = $class->SUPER::new( %h_param );

	return $self;
}


=head2 Function IsFatal

 Title      : IsFatal
 Usage      : my $bool = $err->IsFatal()
 Prerequiste: Fatal code are odd code value
 Function   : tell if the program must stop
 Returns    : a boolean, 1 if value is FATAL
 Args       : none
 Globals    : &FATAL

=cut

sub IsFatal
{
	my $self = shift;

	return 0	unless( defined( $self->{ -value } ) );

	return ( $self->{ -value } & &FATAL );
}


=head2 Function __GetBaseMessage

 Title      : __GetBaseMessage
 Usage      : my $msg = $err->__GetBaseMessage()
 Prerequiste: none
 Function   : give a formatted msg containing where the error occurred
 Returns    : a string
 Args       : none
 Globals    :    none

=cut

sub __GetBaseMessage
{
	my $self = shift;

	# Construct message
	my $ref = ref($self);
	my $msg = "$ref thrown by $self->{ -file } on line $self->{ -line }\n";

	return $msg;
}


=head2 function _GetDebugMessage

 Title        : _GetDebugMessage
 Usage        : $dbg_msg = $o_err->_GetDebugMessage();
 Prerequisite : can be override, to give a more specific message to developpers.
 Function     : accessor for _debug attribute
 Returns      : a string
 Args         : none
 Globals      : none

=cut

sub _GetDebugMessage
{
	my $self = shift;

	return ( defined $self->{ _debug } ) ? $self->{ _debug } : '';
}


=head2 Function Explain

 Title      : Explain
 Usage      : print STDERR $err->Explain()
 Prerequiste: should be final, please try not to override it
 Function   : it returns a standardized string, explaining the error.
              Those information are for developpers.
 Returns    : a string, the error message like :
              "Error thrown by x on line x       # <-- standard
               your code is not fullproof yet    # <-- -text message
               try again !"
 Args       : none
 Error      : none
 Globals    : none

=cut

sub Explain
{
	my $self   = shift;

	my $msg = $self->__GetBaseMessage() . ' '.$self->text();
	$msg   .= ' ' . $self->_GetDebugMessage();

	return $msg
}


=head2 function

 Title        : GetUserMessage
 Usage        : $string = $o_xx->GetUserMessage( none );
 Prerequisite : none
 Function     : return the -user_msg attribute.
 Returns      : $string
 Args         : none

=cut

sub GetUserMessage
{
	my $self = shift;

	my $msg = $self->{ -user_msg };

	return ( $msg eq '' ) ? $self->text() : $msg;
}

=pod

=head1 COPYRIGHT NOTICE

This software is governed by the CeCILL license - www.cecill.info

=cut

1;
