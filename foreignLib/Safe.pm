package Safe;

use IO::File;
use IO::Zlib;
use Data::Dumper;

#Lipm
use LipmError;

sub newIOFile
{
	my( $file, $mode ) = @_;

	my $o_fe;
	if( $file =~ /\.gz$/ )
	{
		$o_fe = new IO::Zlib( $file, $mode.'b' );
	}
	else
	{
		$o_fe = new IO::File( $file, $mode );
	}

	if( !$o_fe )
	{
		my $msg = "File '$file' cannot be openned in '$mode' mode.\n";
		throw LipmError( -text => $msg, -depth => 1 );
	}

	return $o_fe;
}

1;

__END__
