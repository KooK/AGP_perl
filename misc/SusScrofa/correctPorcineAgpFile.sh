#!/bin/sh

# usage : correctPorcineAgpFile.sh
# the Sus scrofa AGP file provided by the consortium is malformatted
# this script correct it by adding the length of the gap and the empty field on "gap line".
# On assembly 10.2v17 it was not necessary.

#ROOT=/ext/nar6/our_DATA/FromPublicData_WithPublicProgram/Pig_Assembly
#PIG_AGP_DIR=$ROOT/Sscrofa10_pre_agp
#PIG_AGP_CORRECTED_DIR=$ROOT/Sscrofa10_pre_agp_corrected
PIG_AGP_DIR=/home/seb/Bureau/Seb/data/Sus_scrofa_agp
PIG_AGP_CORRECTED_DIR=/home/seb/Bureau/Seb/data/Sus_scrofa_agp_corrected

mkdir -p $PIG_AGP_CORRECTED_DIR

for chrom in `seq -s" " 1 18` U X Y
# for chrom in Y
do
	printf "chrom = $chrom\n"
#	chrom_file=chromosome_$chrom.agp
	chrom_file=chr$chrom.agp
	infile=$PIG_AGP_DIR/chr$chrom.agp

	if [ ! -e $infile ]
	then
#		printf "%s does not exist\n" $chrom_file
		continue;
	fi

	perle='if($F[4] eq "N"){'
	perle=$perle' @A[0..4] = @F[0..4];'
	perle=$perle' $A[5] = $F[2]-$F[1]+1;'
	perle=$perle' @A[6..7]=@F[5..6]; $A[8]="";'
	perle=$perle' } else { @A = @F; }'
	perle=$perle' print join("\t", @A );'

	outfile=$PIG_AGP_CORRECTED_DIR/$chrom_file
	printf "%s %s > %s\n" $perle $infile $outfile

	perl -lane "$perle" $infile > $outfile &

# test inutile puisque la commande est lancée en tâche de fond.
#	if [ -e $outfile ]
#	then
#		printf "%s generated.\n" $outfile
#	fi

done
