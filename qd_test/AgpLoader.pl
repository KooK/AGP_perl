#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

use FindBin qw( $RealBin );

use lib "$RealBin/../lib";
use lib "$RealBin/../foreignLib";

use AgpLoader;
use AgpLoader2;
# usage : AgpLoader.pl file.agp

# warn Dumper \%INC;
warn Dumper \@ARGV;


sub print_out
{
	my( $o_agp ) = @_;

	warn "Found " . $o_agp->getNbObjects(). " AGP objects\n";

	my $rh_objects = $o_agp->getObjects();

	for my $name ( keys %$rh_objects )
	{
		my $o_agp_obj = $$rh_objects{ $name };

		my $ra_gaps = $o_agp_obj->getGaps();
		warn "$name has " . scalar @$ra_gaps . " trous.\n";
		for my $o_gap ( @$ra_gaps )
		{
			my( $start, $end ) = $o_gap->getObjectCoords();
			my $length = $o_gap->getLength();

			warn "[$start-$end] ($length)\n";
		}

		my $ra_clones = $o_agp_obj->getClones();
		warn "$name has " . scalar @$ra_clones . " clones.\n";
		for my $o_clone ( @$ra_clones )
		{
			my( $start, $end ) = $o_clone->getCoords();
			my $length = $end - $start +1;

			warn "[$start-$end] ($length)\n";
		}
	}

	return;
}


sub load_class1
{
	my( $file ) = @_;

	my $o_agp = new AgpLoader( $file );
	$o_agp->load();

	print_out( $o_agp );

	return;
}


sub load_class2
{
	my( $file ) = @_;

	my $o_agp = new AgpLoader2( $file );

	print_out( $o_agp );

	return;
}


MAIN:
{
	my $file = $ARGV[0];

#	load_class1( $file );
	load_class2( $file );

	exit( 0 );
}
